const root = document.querySelector('#root');

let fullName = '';


function updateFullName() {
    const avatar = document.querySelector('[data-aid="Avatar__image"]');
    if (avatar)
        fullName = avatar.getAttribute('alt');
}


function setClassOnStoriesWithOwner(name, className) {
    const owners = document.querySelectorAll('.owner');
    for (const owner of owners) {
        const story = owner.parentElement.parentElement.parentElement.parentElement;

        if (owner.getAttribute('title') === name)
            story.classList.add(className);
        else
            story.classList.remove(className);
    }
}


function callback() {
    if (!fullName) {
        // Grab the full name from the avatar alt-text.
        updateFullName();
    }

    setClassOnStoriesWithOwner(fullName, 'owned');
}

root.addEventListener('mouseover', event => {
    if (event.target.className === 'owner') {
        const name = event.target.getAttribute('title');
        setClassOnStoriesWithOwner(name, 'highlighted');
    }
});

root.addEventListener('mouseout', event => {
    if (event.target.className === 'owner')
        setClassOnStoriesWithOwner('', 'highlighted');
});

const observer = new MutationObserver(callback);
const config = { childList: true, subtree: true };
observer.observe(root, config);
